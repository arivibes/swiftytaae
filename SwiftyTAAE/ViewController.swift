//
//  ViewController.swift
//  SwiftyTAAE
//
//  Created by Ariel Elkin on 08/04/2015.
//  Copyright (c) 2015 Ariel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var ae: AudioEngine?

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        ae = AudioEngine()

    }
}

class AudioEngine {

    var audioController: AEAudioController
    var blockChannel: AEBlockChannel


    init() {


        //Let's try to generate and play back a sine wave:
        var sampleRate:Float = 44100.0

        audioController = AEAudioController(audioDescription: AEAudioController.nonInterleaved16BitStereoAudioDescription())

        blockChannel = AEBlockChannel(block:{
            (time:UnsafePointer<AudioTimeStamp>,
            frames:UInt32,
            audio:UnsafeMutablePointer<AudioBufferList>) -> Void in

            for ( var i = UInt32(0); i<frames; i++ ) {

                //generate a sample of sine wave:
                var sineWaveSample = sinf(441.0*Float(i)*2*Float(M_PI)/sampleRate)

                //Now how do I place the sine waves samples in the buffer?


                let buffers = UnsafeBufferPointer<AudioBuffer>(start: &audio.memory.mBuffers,
                    count: Int(audio.memory.mNumberBuffers))

                for buffer:AudioBuffer in buffers {
                    var audioData: UnsafeMutablePointer<Void> = buffer.mData
                    //the line below generates a compiler error:
                    //audioData[i] = 0.0
                }
            }

        })


        audioController.addChannels([blockChannel])

        var error: NSError?
        audioController.start(&error)

        if error != nil {
            println("audio controller start error: \(error)")
        }
    }

}